//
//  AuthViewController.swift
//  VKApp
//
//  Created by Тимур Чеберда on 03.08.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    private var authService: AuthService!
    
    @IBAction func signInTouch(_ sender: UIButton) {
        authService.wakeUpSession()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        authService = AuthService()
        authService = AppDelegate.shared().authServices
        // Do any additional setup after loading the view.
    }
}
