//
//  API.swift
//  VKApp
//
//  Created by Тимур Чеберда on 03.08.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import Foundation

struct API {
    static let scheme = "https"
    static let host = "api.vk.com"
    static let version = "5.92"
    
    static let newsFeed = "/method/newsfeed.get"
}
