//
//  UIViewController + Storyboard.swift
//  VKApp
//
//  Created by Тимур Чеберда on 03.08.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    class func loadFromStoryboard<T: UIViewController>() -> T {
        let name = String(describing: T.self)
        let storyboard = UIStoryboard(name: name, bundle: nil)
        if let viewController = storyboard.instantiateInitialViewController() as? T {
            return viewController
        } else {
            fatalError("Error: No initial ViewController in \(name) storyboard")
        }
    }
}
